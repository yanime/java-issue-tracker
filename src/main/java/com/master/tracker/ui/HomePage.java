package com.master.tracker.ui;

import com.master.tracker.core.Issue;
import com.master.tracker.dao.IssueDao;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.awt.*;
import java.util.Date;
import java.util.List;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

    @SpringBean
    IssueDao dao;

	public HomePage(final PageParameters parameters) {
		super(parameters);
        List issueList = dao.getAllIssues();

        IModel timeStampModel = new Model<String>(){
            @Override
            public String getObject() {
                return new Date().toString();
            }
        };
        add(new Label("timeStamp", timeStampModel));


        add(new ListView<Issue>("issues", issueList) {
            @Override
            protected void populateItem(ListItem<Issue> item) {
                item.add(new Label("name", new PropertyModel(item.getModel(), "name")));
            }
        });

    }
}
