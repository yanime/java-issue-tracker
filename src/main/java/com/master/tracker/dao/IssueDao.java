package com.master.tracker.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import com.master.tracker.core.Issue;

import javax.annotation.Resource;
import java.util.List;

public class IssueDao {
    @Resource
    SessionFactory sessionFactory;

    @Transactional(readOnly = true)
    public Issue getIssue(Long id) {
        return (Issue) sessionFactory.getCurrentSession().createCriteria(Issue.class).add(Restrictions.eq("id", id)).uniqueResult();
    }
    @Transactional
    public void saveIssue(Issue issue) {
        sessionFactory.getCurrentSession().save(issue);
    }


    @Transactional
    @SuppressWarnings("unchecked")
    public List<Issue> getAllIssues() {
        List<Issue> issueList = (List<Issue>) sessionFactory.getCurrentSession()
                .createCriteria(Issue.class).list();

        return issueList;
    }


    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}