package com.master;

import com.master.tracker.core.Issue;
import com.master.tracker.dao.IssueDao;
import junit.framework.TestCase;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext.xml"})
public class AppTest
        extends TestCase {

    @Before
    public void before() {

    }

    @Autowired
    IssueDao issueDao;


    @org.junit.Test
    public void testIssues() throws Exception {
        Issue i = new Issue();
        i.setName("name");
        i.setDescription("afsafd");
        issueDao.saveIssue(i);
    }

    @org.junit.Test
    public void getAllIssues() throws Exception {
        List Issues = issueDao.getAllIssues();
        assertNotNull(Issues);
    }
}
